require("dotenv").config();
var express = require("express");
var api = express();
var server = require('http').createServer(api);
var mongoose = require("mongoose")
var path = require("path");
var body_parser = require('body-parser');

server.listen(process.env.API_PORT, (err,res)=>{
    if(err) console.log("Error del servidor"+err);
    else console.log("API conectado");
})


mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
    if (err) console.log(`ERROR: connecting to Database. ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

var apiRoutes = require("./app/routes/api");
api.use(body_parser.urlencoded({extended:true}));
api.use("/", apiRoutes);