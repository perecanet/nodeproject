var mongose = require('mongoose'),
    Schema = mongose.Schema;

    var chatShema = new Schema({
        user: {type: String},
        sala: {type: String},
        msg: {type: String},
        data: {type: String},
        create_at: {type: Date, default: Date.now},
    });

module.exports = mongose.model("Chat", chatShema);
