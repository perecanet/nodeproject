var mongose = require('mongoose'),
    Schema = mongose.Schema;

    var inidenciaShema = new Schema({
        usuari: {type: String},
        titol: {type: String},
        tipo: {type: String},
        urgencia: {type: String},
        descripcio: {type: String}
    });

module.exports = mongose.model("Incidencia", inidenciaShema);
