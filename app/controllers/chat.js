var mongooose = require("mongoose"),
Chat = require("../models/chat");

exports.load = async () => {
   var res= await Chat.find();
   return res;
};

exports.loadchat = async (idSala) => {
    var res = await Chat.find({
        $or: [
            {sala : idSala},
            {sala : "all"}
        ]
    });
    return res;
}

exports.save = async (req)=>{
    console.log(req);
    var newChat = new Chat(req);
    newChat.save((err,res)=>{
        if(err)console.log(err);
        console.log("Insertado en BD");
        return res;
    })
}

