var mongooose = require("mongoose"),
    Incidencia = require("../models/Incidencia");
exports.load = async () => {
   var res = await Incidencia.find({});
   return res;
};

exports.findOne = async(id) => {  
    var res = await Incidencia.findById(id);
    return res;
};
exports.findbyUser = async(user) => {
    var result = await Incidencia.find({ "usuari" : {$regex: '.*'+user+'.*'}});
    return result;
};

exports.save = (req)=>{
    var newIncidencia = new Incidencia(req);
    newIncidencia.save((err,res)=>{
        if(err)console.log(err);
        else console.log("Insertado en BD");
        return res;
    })
}

exports.delete = async(id) => {
    var res = await Incidencia.findByIdAndRemove(id);
    return res;
};

exports.modify = async(idmod, modificado)=>{
    var res = await Incidencia.findOneAndUpdate(idmod, modificado);
    return res;
};

