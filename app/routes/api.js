var path = require("path");
var express = require("express");
var router = express.Router();
var bodyParser = require('body-parser');
var chatCntr = require(__dirname+"/../controllers/chat")
var IncidenciaCntr = require(__dirname+"/../controllers/incidencia")
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })


router.route("/incidencia/llista").get(async(req,res,next) => {
    var result = await IncidenciaCntr.load();
    var resultat = { llista: result};
    console.log("resultat");
    res.jsonp(resultat);
});

router.route("/chat").get(async(req,res,next) => {
    var result = {msgs:await chatCntr.load(req.query.id)};
    res.jsonp(result);
});

router.post("/incidencia/nova/guardar",urlencodedParser,function (req, res, next){
    console.log(req.body);
    var incidencia = {
        usuari: req.body.name,
        dni: req.body.dni,
        tipo: req.body.type,
        urgencia: req.body.priority,
        descripcio: req.body.desc
    };
    console.log(incidencia);
    IncidenciaCntr.save(incidencia);

});

router.route("/chat/new").get((req,res,next) =>{

    var newChat = {
        user: '/user/2',
        sala: '2',
        msg: "hola",
        date: "15:15"
    }; 
    chatCntr.save(newChat);
})


module.exports = router;