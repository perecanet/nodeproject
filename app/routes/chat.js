var express = require("express");
var router = express.Router();
var chatCntr = require(__dirname+"/../controllers/chat")


router.route("/").get(async(req,res,next) => {
    var result = await chatCntr.loadchat("all");
    var mensages = {msgs:result};
    res.render("chat",mensages);
});

router.get("/:id", async (req, res, next) => {
    var sala = req.params.id;
    var result = await chatCntr.loadchat(sala);
    var mensages = {msgs:result};
    res.render("chat",mensages);
});




module.exports = router;