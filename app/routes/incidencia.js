var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
var incidenciaCtrl = require(path.join(ctrlDir,"incidencia"));
var body_parser = require('body-parser');
var urlEncoder = body_parser.urlencoded({extended:false});

router.get("/", async (req, res, next) => {
    var myData = await incidenciaCtrl.load();
    console.log(myData);
    res.render("llistaincidencies", {llistaincidencies:myData});
    console.log("se esta consultando");
});

router.get("/nova",function (req, res, next){
    res.render("novaincidencia",  {title: "Express"});
});

router.get("/:id", async function (req, res, next){
    var myData = await incidenciaCtrl.findOne(req.params.id);
    console.log(myData);
    res.render("incidenciaUnica",  {title: "Express", myData:myData});
});

router.get("/:id/modificar",async function (req, res, next){
    var myData = await incidenciaCtrl.findOne(req.params.id);
    res.render("incidenciaModificar",  {title: "Express", myData:myData});
});

router.post( "/:id/modificar/save", urlEncoder,async (req,res,next) => {
    const idModif = req.params.id;
    console.log("Dins save de modify");
    console.log(req.body);
    var data = await incidenciaCtrl.modify(idModif,req.body);
    console.log("data : "+data);
    if (!data) {
        res.status(404).send({
          message: 'Cannot modify assignatura with id=${idmod}. Maybe Tutorial was not found!'
        });
      } else {
        res.redirect("http://localhost:3030/incidencia/");
      }
});


router.get("/:id/eliminar", async function (req, res, next){
    await incidenciaCtrl.delete(req.params.id);
    res.redirect("http://localhost:3030/incidencia/");
});


router.post("/nova", urlEncoder, (req, res, next) => {
    var newIncidencia = {
        usuari: req.body.dni,
        titol: req.body.name,
        tipus: req.body.type,
        urgencia: req.body.priority,
        estat: "pendent",
        descripcio: req.body.desc

    };
    incidenciaCtrl.save(newIncidencia);
    res.redirect("http://localhost:3030/incidencia/");
});

router.post("/filtreUsuari", urlEncoder, async (req,res,next) => {
    var usuari = req.body.user;
    console.log(usuari);
    var data = await incidenciaCtrl.findbyUser(usuari);
    if(data){
        res.json({msg:'success', incidencies:data}); 
    }
    else{
        res.json({msg:'error'}); 
    }

})

module.exports = router;
