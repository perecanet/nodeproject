$(document).ready(function(){ 

    $('#search').click(function(e){
         e.preventDefault();
         var user = $("#user").val();
         console.log("User "+user); 
       $.ajax({  
           url:'/incidencia/filtreUsuari',  
           method:'post',  
           dataType:'json',  
           data:{'user':user},  
           success:function(response){
               $('#llistaincidencies').empty();
               if(response.msg=='success'){  
                response.incidencies.forEach(element => {
                    console.log(element);
                    savetoList(element);
                });
               }else{  
                   alert('some error occurred try again');  
               }  
           },  
           error:function(response){  
               alert('server error occured')  
           }  
       });  
    }); 
    
    function savetoList(inciden){
        $('#llistaincidencies').append('<div class="col-xs-12 col-md-4 mt-5"><h3 class="titolIncidencia">'+inciden.titol+'</h3><div class="card"><img class="card-img-top" alt="Card image cap" src="/img/incidencia.png" heigth="200"><div class="card-body"><h5 class="card-title> Urgència: '+inciden.urgencia+' </h5><p class="card-text>  '+inciden.descripcio+'</p><p class="card-text"> Usuari : '+inciden.usuari+'</p><a class="btn btn-primary" href="/incidencia/'+inciden._id+'">Veure incidencia</a></div></div></div>');
    }
});  