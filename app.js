require("dotenv").config();
var express = require("express");
var app = express();
var server = require('http').createServer(app);
var mongoose = require("mongoose")
var path = require("path");
var io = require("socket.io")(server)
var chatCntr = require(__dirname+"/app/controllers/chat")

server.listen(process.env.SERVER_PORT, (err,res)=>{
    if(err) console.log("Error del servidor"+err);
    else console.log("Servidor conectado");
})




mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
    if (err) console.log(`ERROR: connecting to Database. ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

app.set("views",__dirname + '/views');
app.set("view engine","pug");
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));

var indexRouter = require("./app/routes/base");

var chatRoutes = require("./app/routes/chat");


var incidenciaRoutes = require("./app/routes/incidencia");

app.use("/", indexRouter);
app.use("/chat", chatRoutes);
app.use("/incidencia", incidenciaRoutes);

io.on("connection",(socket) => {
    console.log("Conectado al socket");
    socket.on("newMsg1",(data)=> {
        chatCntr.save(data);
      socket.broadcast.emit("newMsg1",data)
    })
    socket.on("newMsg2",(data)=> {
        chatCntr.save(data);
      socket.broadcast.emit("newMsg2",data)
    })
    socket.on("newMsg3",(data)=> {
        chatCntr.save(data);
      socket.broadcast.emit("newMsg3",data)
    })
    socket.on("newMsgall",(data)=> {
        chatCntr.save(data);
      socket.broadcast.emit("newMsgall",data)
    })
})